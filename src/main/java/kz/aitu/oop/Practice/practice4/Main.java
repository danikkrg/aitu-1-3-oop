package kz.aitu.oop.Practice.practice4;

public class Main {
    public static void main(String[] args) {
        int totalPriceOfFish = 0;
        int totalPriceOfReptile = 0;

        Aquarium[] aquariumElements = new Aquarium[4];
        aquariumElements[0] = new Fish("fish1", 100);
        aquariumElements[1] = new Fish("fish2", 500);
        aquariumElements[2] = new Fish("fish3", 2000);
        aquariumElements[3] = new Reptile("rept", 1000);

        for (Aquarium aquariumElement: aquariumElements) {
            if (aquariumElement instanceof Fish) {
                totalPriceOfFish += aquariumElement.getCost();
            }

            if (aquariumElement instanceof Reptile) {
                totalPriceOfReptile += aquariumElement.getCost();
            }
            System.out.println(totalPriceOfFish);
            System.out.println(totalPriceOfReptile);
        }
    }
}