package kz.aitu.oop.Practice.practice3;


public class Main {
    public static void main(String[] args) {
        Company company = new Company();
        System.out.println(company.toString());

        int total = 0;

        Company [] companies = new Company[3];
        companies[0] = new Employee(400000);
        companies[1] = new Employee(280000);
        companies[2] = new Employee(150000);

        for (Company companyElement: companies) {
            if (companyElement instanceof Employee) {
                total += ((Employee) companyElement).getSalary();
            }
        }
        total += company.getMaterialCosts();
        System.out.println("finally: " + total);
    }
}