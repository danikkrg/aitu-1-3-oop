package kz.aitu.oop.Practice.practice5;

public class Main {
    public static void main(String[] args) {
        double totalPS = 0, weightPS = 0;
        double totalSPS = 0, weightSPS = 0;

        Stone[] stones = new Stone[4];
        stones[0] = new Precious(9.5, 300000);
        stones[1] = new SemiPrecious(12.4, 400000);
        stones[2] = new Precious(7.3, 180000);
        stones[3] = new SemiPrecious(21.19, 900000);

        for (Stone stoneElement: stones) {
            if (stoneElement instanceof Precious) {
                weightPS += stoneElement.getWeight();
                totalPS += stoneElement.getCost();
            }

            if (stoneElement instanceof SemiPrecious) {
                weightSPS += stoneElement.getWeight();
                totalSPS += stoneElement.getCost();
            }
            System.out.println("Total price of precious stone: " + totalPS + " KZT" + ", weight: " + weightPS + " carats");
            System.out.println("Total price of semi - precious stone: " + totalSPS + " KZT" + ", weight: " + weightSPS + " carats");
        }
    }
}