package kz.aitu.oop.Practice.practice2;

public class Main {
    public static void main(String[] args) {
        Freight f = new Freight(500, "black", 20, 200.0, 500.0);
        f.transport();
        f.transport(200.0);

        Locomotive locomotive = new Locomotive("yellow", 15, 300.0, 1700.0);
        Talgo talgo = new Talgo(1000, "blue", 20, 500.0, 400.0);

        Train[] trains = new Train[5];

        System.out.println(f.toString());
        System.out.println(locomotive.toString());
        System.out.println(talgo.toString());
    }
}
