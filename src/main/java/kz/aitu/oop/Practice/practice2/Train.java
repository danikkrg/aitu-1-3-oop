package kz.aitu.oop.Practice.practice2;


public class Train {
    public String color;
    public int coachCount;
    public double speed;
    public double length;

    public Train(String color, int coachCount, double speed, double length) {
        this.color = color;
        this.coachCount = coachCount;
        this.speed = speed;
        this.length = length;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCoachCount() {
        return coachCount;
    }

    public void setCoachCount(int coachCount) {
        this.coachCount = coachCount;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public int addCoach() {
        return coachCount++;
    }
}