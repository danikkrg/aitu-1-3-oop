package kz.aitu.oop.examples.assignment3;

import java.io.*;
import java.util.Scanner;

public class MyString {

    public static int[] getArr() {
        return arr;
    }

    public static void setArr(int[] arr) {
        MyString.arr = arr;
    }

    private static int[] arr;
    private static String fileName;

    public static void WriteObjectToFile(MyString str) {

        Scanner sc = new Scanner(System.in);
        String c = sc.next();
        try {
            fileName = "/Users/Данияр/Desktop/" + c + ".txt";
            new FileOutputStream(fileName);
        } catch (IOException e) {
        }
        try(FileWriter fw = new FileWriter(fileName, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            int[] a = str.getArr();
            for (int i = 0; i < a.length-1; i++ ) {
                out.print(a[i] + ", ");
            }
            out.println(a[a.length-1]);
        } catch (IOException e) {
            //something should be
        }
    }
    public Object ReadObjectFromFile(MyString str) throws FileNotFoundException {

        File file = new File(fileName);
        Scanner sc = new Scanner(file);
        int[] a = new int[length()];
        int i = 0;
        while (sc.hasNextInt()) {
            a[i] = sc.nextInt();
            i++;
        }
        MyString New = new MyString(a);
        return New;
    }
    public MyString(int[] values) {
        arr = values;
    }
    public MyString() {
    }
    public int length() {
        return arr.length;
    }
    public int valueAt(int position) {
        if (position >= 0 && position < arr.length) {
            return arr[position];
        } else {
            return -1;
        }
    }

    public boolean contains(int value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                return true;
            }
        }
        return false;
    }
    // count for how many time value is stored
    public int count(int value) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                count++;
            }
        }
        return count;
    }
    public void printValues() {
        for (int i = 0; i < arr.length; i++ ) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
