package kz.aitu.oop.examples.Assignment7.Subtask3;

public class Main {

    public static void main(String[] args) {
        Circle s1 = new Circle(5.5);
        System.out.println(s1);
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());
        s1.setRadius(2);
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());

        ResizableCircle s2 = new ResizableCircle(2);
        System.out.println(s2);
        System.out.println(s2.getArea());
        System.out.println(s2.getPerimeter());
        s2.resize(200);
        System.out.println(s2.getArea());
        System.out.println(s2.getPerimeter());
    }
}