package kz.aitu.oop.examples.Assignment7.Subtask2;

public interface Movable {

    public void moveRight();
    public void moveLeft();
    public void moveUp();
    public void moveDown();
}