package kz.aitu.oop.examples.Practice5;

import java.io.File;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        String str = "3.5";
        System.out.println(fileExists("C:\\Users\\Данияр\\Desktop\\Lecture 5 - Exception handling.pdf"));
        System.out.println(isInt(str));
        System.out.println(isDouble(str));
    }
    // Task 1 fileExists():

    public static boolean fileExists(String path){
        try {
            File file = new File(path);
            Scanner sc = new Scanner(file);
        } catch (Exception e){
            return false;
        }
        return true;
    }




    //Task 2 isInt():
    public static boolean isInt(String str) {
        try {
            int num = Integer.parseInt(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    //Task 3 isDouble():
    public static boolean isDouble(String str) {
        try {
            double numb = Double.parseDouble(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}