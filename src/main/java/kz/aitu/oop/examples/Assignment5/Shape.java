package kz.aitu.oop.examples.Assignment5;

public class Shape {
    private String color;
    private boolean filled;

    public Shape(){
        filled = true;
        color = "green";
    }

    public Shape (String newColor,boolean filledNew){
        filled = filledNew;
        color = newColor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        if (filled == true){
            return true;
        }
        else{
            return false;
        }
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString() {
        String isNot =" ";
        if(isFilled() == false) {
            isNot = "Not";
        }
        return "A shape with color of " + color + " and " + isNot + " filled.";
    }
}
