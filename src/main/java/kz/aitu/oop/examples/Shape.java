package kz.aitu.oop.examples;

import java.util.List;

public class Shape {
    private List<Point> pts;

    public void setpts(List<Point> ps) {
        pts = ps;
    }

    public void addPoint(Point p) {
        pts.add(p);
    }

    public List<Point> getPoint() {
        return pts;
    }

    public int calculatePerimeeter() {
        int answer = 0;
        for (int i = 0;i<pts.size()-1;i++) {
            Point p1 = pts.get(i);
            Point p2 = pts.get(i+1);
            answer += p1.distance((p2));
        }

        Point first = pts.get(0);
        Point last = pts.get(pts.size()-1);
        answer += first.distance(last);
        return answer;
    }

    public double Longest() {
        double[] answer = new double[pts.size()+1];
        Point first = pts.get(0);
        Point last = pts.get(pts.size()-1);
        answer[0] = first.distance(last);
        int j = 1;
        for (int i = 0;i < pts.size()-1;i++) {
            Point p1 = pts.get(i);
            Point p2 = pts.get(i+1);
            answer[j] = p1.distance(p2);
            j++;
        }
        double max = answer[0];
        for (int i = 1;i<answer.length;i++) {
            if (answer[i] > max) {
                max = answer[i];
            }
        }
        return max;
    }

    public double Average() {
        double answer = 0;
        Point first = pts.get(0);
        Point last = pts.get(pts.size()-1);
        answer += first.distance(last);
        for (int i = 0;i < pts.size()-1;i++) {
            Point p1 = pts.get(i);
            Point p2 = pts.get(i+1);
            answer += p1.distance(p2);
        }
        answer /= (pts.size()+1);
        return answer;
    }
}
