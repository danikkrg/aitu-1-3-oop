package kz.aitu.oop.examples;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class AssignmentOne {
    public static void main() throws Exception {
        AssignmentOne fileReader1 = new AssignmentOne();

        fileReader1.getFile().getPoint();
    }

    public Shape getFile() throws FileNotFoundException {
        File file = new File("/Users/Данияр/Desktop/file2.txt");
        Scanner scanner = new Scanner(file);
        Shape shape = new Shape();
        Double x= null;
        Double y= null;

        while (scanner.hasNext()) {
            if (scanner.hasNextFloat()) {
                if (x == null) {
                    x = scanner.nextDouble();
                    System.out.println("X is " + x);
                } else if (y == null) {
                    y = scanner.nextDouble();
                    System.out.println("Y is " + y);
                }
                if (x != null && y != null) {
                    shape.addPoint(new Point(x,y));
                    System.out.println("Point " + x + " " + y + " is added");
                    x = null;
                    y = null;
                }
            }
        }
        return shape;
    }
}
