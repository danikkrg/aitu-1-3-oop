package kz.aitu.oop.examples.quiz.practice7;

public class FoodFactory {

    public Food getFood(String order) {
        if(order.equalsIgnoreCase("cake")) {
            Food cake = new Cake();
            return cake;
        } else {
            Food pizza = new Pizza();
            return pizza;
        }
    }
}