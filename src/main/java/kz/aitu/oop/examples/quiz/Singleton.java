package kz.aitu.oop.examples.quiz;

public class Singleton {

    private static Singleton instance;

    private Singleton() {
    }

    public String str;

    public static synchronized Singleton getSingleInstance() {

        if(instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}