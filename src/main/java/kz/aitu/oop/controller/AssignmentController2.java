package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.examples.Point;
import kz.aitu.oop.repository.StudentFileRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;

import static java.lang.Integer.parseInt;

@RestController
@RequestMapping("/api/task/2")
 public class AssignmentController2 {


    @GetMapping("/students")
    public ResponseEntity<?> getStudents() throws FileNotFoundException {

        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        for (Student student: studentFileRepository.getStudents()) {
            result += student.getName() + "\t" + student.getAge() + "\t" + student.getPoint() + "</br>";
        }

        return ResponseEntity.ok(result);
    }

    /**
     * Method get all Students from file and calculate average name lengths
     * @return average name length of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsNameLength")
    public ResponseEntity<?> averageStudentsNameLength() throws FileNotFoundException {

        double average = 0;
        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getName().length();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }


    @GetMapping("/averageStudentsCount")
    public ResponseEntity<?> averageStudentsCount() throws FileNotFoundException {

        double count = 0;
        //change your code here

        int group = 0;
        int counter = 0;
        int groupCounter = 0;

        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student: studentFileRepository.getStudents()) {
            count++;
        }
        for (Student student: studentFileRepository.getStudents()){
            String groupStr = student.getGroup();
            if (group < parseInt(groupStr.substring(2))) {
                group = parseInt(groupStr.substring(2));
                groupCounter++;
            }
            counter++;
            }
            count = count / groupCounter;
            return ResponseEntity.ok(count);
    }

    @GetMapping("/averageStudentsPoint")
    public ResponseEntity<?> averageStudentsPoint() throws FileNotFoundException {

        double average = 0;
        //change your code here

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getPoint();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }

    @GetMapping("/averageStudentsAge")
    public ResponseEntity<?> averageStudentsAge() throws FileNotFoundException {

        double average = 0;
        //change your code here

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getAge();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }

    @GetMapping("/highestStudentsPoint")
    public ResponseEntity<?> highestStudentsPoint() throws FileNotFoundException {
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double maxPoint = 0;
        //change your code here

        double total = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total = student.getPoint();
            if (maxPoint < total) {
                maxPoint = total;
            }
        }
            return ResponseEntity.ok(maxPoint);
    }

    @GetMapping("/highestStudentsAge")
    public ResponseEntity<?> highestStudentsAge() throws FileNotFoundException {
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        double maxAge = 0;
        //change your code here

        double total = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total = student.getAge();
            if (maxAge < total) {
                maxAge = total;
            }
        }
        return ResponseEntity.ok(maxAge);
    }

    @GetMapping("/highestGroupAveragePoint")
    public ResponseEntity<?> highestGroupAveragePoint() throws FileNotFoundException {

        double averageGroupPoint = 0;
        //change your code here

        double counter = 0;
        double group = 0;
        double groupCounter = 0;
        double total = 0;
        double groupPoint = 0;
        int count = 0;

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        for (Student student: studentFileRepository.getStudents()) {
            String groupStr = student.getGroup();
            if (group < parseInt(groupStr.substring(2))) {
                group = parseInt(groupStr.substring(2));
                averageGroupPoint = total / count;
                total = 0;
                count = 0;
                if (groupPoint < averageGroupPoint) {
                    groupPoint = averageGroupPoint;
                }
            }
            total += student.getPoint();
            count++;
            counter++;
        }

        return ResponseEntity.ok(averageGroupPoint);
    }

    @GetMapping("/highestGroupAverageAge")
    public ResponseEntity<?> highestGroupAverageAge() throws FileNotFoundException {

        double averageGroupAge = 0;
        //change your code here

        double group = 0;
        double counter = 0;
        double groupCounter = 0;
        double total = 0;
        double groupAge = 0;
        int count = 0;

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        for (Student student: studentFileRepository.getStudents()) {
            String groupStr = student.getGroup();
            if (group < parseInt(groupStr.substring(2))) {
                group = parseInt(groupStr.substring(2));
                averageGroupAge = total / count;
                total = 0;
                count = 0;
                if (groupAge < averageGroupAge) {
                    groupAge = averageGroupAge;
                }
            }
            total += student.getAge();
            count++;
            counter++;
        }

        return ResponseEntity.ok(averageGroupAge);
    }


    @GetMapping("/all")
    public ResponseEntity<?> allData() throws FileNotFoundException {

        String result = "";
        result += "averageStudentsNameLength: " + averageStudentsNameLength().getBody() + "</br>";
        result += "averageStudentsCount: " + averageStudentsCount().getBody() + "</br>";
        result += "averageStudentsPoint: " + averageStudentsPoint().getBody() + "</br>";
        result += "averageStudentsAge: " + averageStudentsAge().getBody() + "</br>";
        result += "highestStudentsPoint: " + highestStudentsPoint().getBody() + "</br>";
        result += "highestStudentsAge: " + highestStudentsAge().getBody() + "</br>";
        result += "highestGroupAveragePoint: " + highestGroupAveragePoint().getBody() + "</br>";
        result += "highestGroupAverageAge: " + highestGroupAverageAge().getBody() + "</br>";

        return ResponseEntity.ok(result);
    }
}
